// Unity Script to place a geopoint (lat,lng) over a 3d sphere
using UnityEngine;
using System.Collections;

public class gpsToSphere : MonoBehaviour {
	public Transform myobject;
	public float radius = 5f; 
	public float latitude = 0f; 
	public float longitude = 0f; 
	float width = 1.0f;
	float xPos;
	float yPos;
	float zPos;

	void Start () {
		latitude = Mathf.PI  * latitude / 180;
		longitude = Mathf.PI  * longitude / 180;
		latitude -= 1.570795765134f; 
		xPos = (radius) * Mathf.Sin(latitude) * Mathf.Cos(longitude);
		zPos = (radius) * Mathf.Sin(latitude) * Mathf.Sin(longitude);
		yPos = (radius) * Mathf.Cos(latitude); 
		myobject.position = new Vector3(xPos,yPos,zPos);
	}

}